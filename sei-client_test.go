package soapclient

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"gitlab.com/shadowy/go/utils"
	"gitlab.com/shadowy/sei/common/wsdl"
	"testing"
	"time"
)

const systemID = "c6ca6f5c-5d90-1383-1f96-d3481cb4dcea"
const password = "e6802513-409a-f666-6fbc-4dfe7d2f4475"
const orgFrom = "from"
const orgTo = "to"
const seiSystemID = "99999999"
const testData = "1234567890"

var client = GetSEIClient(&SEISettings{
	URL:      "https://sei.shadowy.eu/soap/action",
	SystemID: systemID,
	Password: password,
})

func TestSEIClient_GetInputMessages(t *testing.T) {
	res, err := client.GetInputMessages(10)
	if err != nil {
		t.Error(err)
	} else {
		t.Logf("Count: %d", len(res))
	}
}

func TestSEIClient_GetVersion(t *testing.T) {
	res, err := client.GetVersion()
	if err != nil {
		t.Error(err)
	} else {
		t.Logf("Version: %s", *res)
	}
}

func TestSEIClient_UploadMessageChunk(t *testing.T) {
	data := testData
	var create = wsdl.DateTime(time.Now().Format("2006-01-02T15:04:05.000000"))
	var creator = wsdl.MessageCreator("SEI-Email")
	var format = wsdl.DataTransferFormat("Plain")
	var tp = wsdl.MessageType("Document")
	msgID := guid()
	fileInfo := wsdl.MessageInfo{
		CreationDate: &create,
		Creator:      &creator,
		Format:       &format,
		FromOrgId:    utils.PtrString(orgFrom),
		FromSysId:    utils.PtrString(systemID),
		MessageId:    &msgID,
		Size:         utils.PtrInt64(int64(len(data))),
		ToOrgId:      utils.PtrString(orgTo),
		ToSysId:      utils.PtrString(seiSystemID),
		Type:         &tp,
	}
	res, err := client.OpenUploadingSession(&fileInfo, Hash(data), "")
	if err != nil {
		t.Error(err)
		return
	}
	sessionID := res.SessionId
	t.Logf("sessionID = %d", *sessionID)
	d := base64.StdEncoding.EncodeToString([]byte(data))
	res, err = client.UploadMessageChunk(sessionID, d)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("UploadMessageChunk ok")
}

func TestSEIClient_DownloadMessageChunk(t *testing.T) {
	res, err := client.GetInputMessages(10)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("Count: %d", len(res))
	if len(res) == 0 {
		t.Skip("No data")
		return
	}
	sessionID := res[0].SessionId
	msgID := res[0].MessageId
	t.Logf("Download msgID = %s sessionID = %d", *msgID, sessionID)
	session, err := client.OpenDownloadingSession(msgID)
	if err != nil {
		t.Error(err)
		return
	}
	sessionID = session.SessionId
	data, err := client.DownloadMessageChunk(sessionID, 0, 2000)
	if err != nil {
		t.Error(err)
		return
	}
	result, _ := base64.StdEncoding.DecodeString(string(*data.MessageChunk))
	d := string(result)
	if d != testData {
		t.Errorf("Not equal \"%s\" != \"%s\"", testData, d)
	}
	status := wsdl.SessionStatus("Delivered")
	session.Status = &status
	session, err = client.EndProcessingDownloadedMessage(session)
	if err != nil {
		t.Error(err)
		return
	}
	st := *session.Status
	t.Logf("UploadMessageChunk ok (status = %s)", string(st))
}

func guid() string {
	b := make([]byte, 16)
	_, _ = rand.Read(b)
	return fmt.Sprintf("%x-%x-%x-%x-%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
}
