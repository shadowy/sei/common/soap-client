package soapclient

import "github.com/sirupsen/logrus"

type SEISettings struct {
	URL        string  `yaml:"url"`
	SystemID   string  `yaml:"systemID"`
	Password   string  `yaml:"password"`
	ToSystemID *string `yaml:"toSystemID"`
}

func (s *SEISettings) Logs() logrus.Fields {
	return logrus.Fields{
		"url":      s.URL,
		"systemID": s.SystemID,
		"password": s.Password,
	}
}
