package soapclient

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/utils"
	"gitlab.com/shadowy/go/xml"
	"gitlab.com/shadowy/sei/common/wsdl"
	"io/ioutil"
	"net/http"
	"strconv"
)

type SEIClient struct {
	cfg  *SEISettings
	http *http.Client
}

func GetSEIClient(cfg *SEISettings) *SEIClient {
	if cfg == nil {
		logrus.Warn("GetSEIClient cfg is nil")
		return nil
	}
	logrus.WithFields(cfg.Logs()).Info("GetSEIClient")
	return &SEIClient{
		cfg:  cfg,
		http: &http.Client{},
	}
}

func (c *SEIClient) DownloadMessageChunk(sessionID *int64, pos, size int64) (result *wsdl.DownloadChunkResponse, err error) {
	logrus.Debug("SEIClient.DownloadMessageChunk")
	r, err := c.callSOAP("tem:DownloadMessageChunk", &wsdl.DownloadMessageChunk{
		Identity:     c.getIdentity(),
		SessionId:    sessionID,
		FromPosition: &pos,
		Count:        &size,
	})
	if err != nil {
		return nil, err
	}
	return r.(*wsdl.DownloadMessageChunkResponse).DownloadMessageChunkResult, nil
}

func (c *SEIClient) EndProcessingDownloadedMessage(session *wsdl.SessionInfo) (result *wsdl.SessionInfo, err error) {
	logrus.Debug("SEIClient.EndProcessingDownloadedMessage")
	r, err := c.callSOAP("tem:EndProcessingDownloadedMessage", &wsdl.EndProcessingDownloadedMessage{
		Identity:    c.getIdentity(),
		SessionInfo: session,
	})
	if err != nil {
		return nil, err
	}
	return c.errorSessionInfo(r.(*wsdl.EndProcessingDownloadedMessageResponse).EndProcessingDownloadedMessageResult)
}

func (c *SEIClient) GetHistory(fromDate, toDate *wsdl.DateTime, count int) (result []*wsdl.MessageInfo, err error) {
	logrus.Debug("SEIClient.GetHistory")
	r, err := c.callSOAP("tem:GetHistory", &wsdl.GetHistory{
		Identity:     c.getIdentity(),
		FromDateTime: fromDate,
		ToDateTime:   toDate,
		Count:        &count,
	})
	if err != nil {
		return nil, err
	}
	return r.(*wsdl.GetHistoryResponse).GetHistoryResult.MessageInfo, nil
}

func (c *SEIClient) GetInputMessages(count int) (result []*wsdl.MessageInfo, err error) {
	logrus.Debug("SEIClient.GetInputMessages")
	r, err := c.callSOAP("tem:GetInputMessages", &wsdl.GetInputMessages{
		Identity: c.getIdentity(),
		Ount:     &count,
	})
	if err != nil {
		return nil, err
	}
	return r.(*wsdl.GetInputMessagesResponse).GetInputMessagesResult.MessageInfo, nil
}

func (c *SEIClient) GetMessageValidationInfo(sessionID int64) (result *wsdl.MessageValidationInfo, err error) {
	logrus.Debug("SEIClient.GetMessageValidationInfo")
	r, err := c.callSOAP("tem:GetMessageValidationInfo", &wsdl.GetMessageValidationInfo{
		Identity:  c.getIdentity(),
		SessionId: &sessionID,
	})
	if err != nil {
		return nil, err
	}
	return r.(*wsdl.GetMessageValidationInfoResponse).GetMessageValidationInfoResult, nil
}

func (c *SEIClient) GetSessionsInfo(ids []*int64) (result []*wsdl.SessionInfo, err error) {
	logrus.Debug("SEIClient.GetSessionsInfo")
	r, err := c.callSOAP("tem:GetSessionsInfo", &wsdl.GetSessionsInfo{
		Identity:    c.getIdentity(),
		SessionsIds: &wsdl.ArrayOflong{Long: ids},
	})
	if err != nil {
		return nil, err
	}
	return r.(*wsdl.GetSessionsInfoResponse).GetSessionsInfoResult.SessionInfo, nil
}

func (c *SEIClient) GetSessionInfo(id *int64) (result *wsdl.SessionInfo, err error) {
	logrus.Debug("SEIClient.GetSessionInfo")
	r, err := c.callSOAP("tem:GetSessionInfo", &wsdl.GetSessionInfo{
		Identity:  c.getIdentity(),
		SessionId: id,
	})
	if err != nil {
		return nil, err
	}
	return c.errorSessionInfo(r.(*wsdl.GetSessionInfoResponse).GetSessionInfoResult)
}

func (c *SEIClient) GetVersion() (result *string, err error) {
	logrus.Debug("SEIClient.GetVersion")
	r, err := c.callSOAP("tem:GetVersion", &wsdl.GetVersion{})
	if err != nil {
		return nil, err
	}
	return r.(*wsdl.GetVersionResponse).GetVersionResult, nil
}

func (c *SEIClient) OpenDownloadingSession(msgID *string) (result *wsdl.SessionInfo, err error) {
	logrus.Debug("SEIClient.OpenDownloadingSession")
	r, err := c.callSOAP("tem:OpenDownloadingSession", &wsdl.OpenDownloadingSession{
		Identity:  c.getIdentity(),
		MessageId: msgID,
	})
	if err != nil {
		return nil, err
	}
	return c.errorSessionInfo(r.(*wsdl.OpenDownloadingSessionResponse).OpenDownloadingSessionResult)
}

func (c *SEIClient) OpenUploadingSession(info *wsdl.MessageInfo, hash, sign string) (result *wsdl.SessionInfo, err error) {
	logrus.Debug("SEIClient.OpenUploadingSession")
	b := []byte(sign)
	r, err := c.callSOAP("tem:OpenUploadingSession", &wsdl.OpenUploadingSession{
		Identity:    c.getIdentity(),
		MessageInfo: info,
		Hash:        &hash,
		Signature:   &b,
	})
	if err != nil {
		return nil, err
	}
	return c.errorSessionInfo(r.(*wsdl.OpenUploadingSessionResponse).OpenUploadingSessionResult)
}

func (c *SEIClient) UploadMessageChunk(sessionID *int64, data string) (result *wsdl.SessionInfo, err error) {
	logrus.Debug("SEIClient.UploadMessageChunk")
	b := []byte(data)
	r, err := c.callSOAP("tem:UploadMessageChunk", &wsdl.UploadMessageChunk{
		Identity:     c.getIdentity(),
		SessionId:    sessionID,
		MessageChunk: &b,
	})
	if err != nil {
		return nil, err
	}
	return c.errorSessionInfo(r.(*wsdl.UploadMessageChunkResponse).UploadMessageChunkResult)
}

func (c *SEIClient) getIdentity() *wsdl.Identity {
	return &wsdl.Identity{
		Password: utils.PtrString(c.cfg.Password),
		SystemId: utils.PtrString(c.cfg.SystemID),
	}
}

func (c *SEIClient) executeHTTP(xml []byte) (res []byte, err error) {
	logrus.WithField("xml", string(xml)).Debug("SEISettings.executeHTTP")
	request, err := http.NewRequest(http.MethodPost, c.cfg.URL, bytes.NewBuffer(xml))
	if err != nil {
		logrus.WithField("url", c.cfg.URL).WithError(err).Error("SEIClient.executeHTTP NewRequest")
		return
	}
	response, err := c.http.Do(request)
	if err != nil {
		logrus.WithField("url", c.cfg.URL).WithError(err).Error("SEIClient.executeHTTP http.Do")
		return
	}
	if response.StatusCode != 200 {
		err = errors.New(strconv.Itoa(response.StatusCode) + " " + response.Status)
		logrus.WithError(err).Error("SEIClient.executeHTTP response.StatusCode")
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logrus.WithField("url", c.cfg.URL).WithError(err).Error("SEIClient.executeHTTP body")
		return
	}

	defer func() {
		_ = response.Body.Close()
	}()

	return body, nil
}

func (c *SEIClient) callSOAP(command string, data interface{}) (res interface{}, err error) {
	logrus.WithField("command", command).Debug("SEISettings.callSOAP")
	msg := wsdl.Envelope{
		Soapenv: "http://schemas.xmlsoap.org/soap/envelope/",
		Tem:     "http://tempuri.org/",
		Cover:   utils.PtrString("http://schemas.datacontract.org/2004/07/Cover.Contracts"),
		Header:  "",
		Body: wsdl.Body{
			Data:    data,
			Command: command,
		},
	}
	d, err := xml.Marshal(&msg)
	if err != nil {
		logrus.WithError(err).Error("SEISettings.callSOAP marshal")
		return nil, err
	}
	r, err := c.executeHTTP(d)
	if err != nil {
		return nil, err
	}
	var result wsdl.Envelope
	err = xml.Unmarshal(r, &result)
	if err != nil {
		logrus.WithError(err).Error("SEISettings.callSOAP unmarshal")
		return nil, err
	}
	return result.Body.Data, nil
}

func (c *SEIClient) errorSessionInfo(result *wsdl.SessionInfo) (*wsdl.SessionInfo, error) {
	if result.Error != nil {
		return nil, errors.New(fmt.Sprintf("%d - %s", *result.Error.Code, *result.Error.Text))
	}
	return result, nil
}
