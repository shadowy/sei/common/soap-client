module gitlab.com/shadowy/sei/common/soap-client

go 1.14

require (
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/shadowy/go/utils v1.0.2
	gitlab.com/shadowy/go/xml v1.0.2
	gitlab.com/shadowy/sei/common/wsdl v0.4.0
)
