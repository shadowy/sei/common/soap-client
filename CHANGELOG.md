# CHANGELOG

<!--- next entry here -->

## 0.1.2
2020-06-25

### Fixes

- extend sei-settings (5caf05a02c467f5d882baee02387eb38b92bbf63)

## 0.1.1
2020-06-23

### Fixes

- update library version (25729cc4507a940e9cce46364b1704b1220f8d1d)

## 0.1.0
2020-06-12

### Features

- add mapping fore response (b7842b3055f5c1e84b9bd0df10774aefae1f6cc6)

### Fixes

- add tests (259b2e0319387fd5f9fd5276c878d9e5d7738c80)